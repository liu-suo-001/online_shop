$(function () {
    $(".imgouter").mouseover(function(){
        // $(".imgbutton").slideDown(200, function () {
            
        // });
        $(".imgbutton").show();
        // $(".imgbutton").animate({"left":50},500);

    })
    $(".imgouter").mouseout(function(){
        $(".imgbutton").hide();
    })

    $(".bodyul li a").click(function(){
        $(this).parent().siblings().children("a").removeClass("myinfo");
        $(this).parent().siblings().children("a").children("span").removeClass("cplolr");
        $(this).addClass("myinfo");
        $(this).children("span").addClass("cplolr");
    })

    $("#choosefalse").click(function(){
      $(".infocontain").css("height","730px");
      $("#choosetrue").show();
      $(".searchrow").show();
      $(this).hide();
    })
    $("#choosetrue").click(function(){
      $("#choosefalse").show();
      $(".searchrow").hide();
      $(".infocontain").css("height","600px");
      $(this).hide();
    })
 
   
    
})
laydate.render({
    elem: '#demoTest' //指定元素
  });



  layui.use(['upload', 'element', 'layer'], function(){
    var $ = layui.jquery
    ,upload = layui.upload
    ,element = layui.element
    ,layer = layui.layer;
    
    //常规使用 - 普通图片上传
    var uploadInst = upload.render({
      elem: '#test1'
      ,url: 'https://httpbin.org/post' //此处用的是第三方的 http 请求演示，实际使用时改成您自己的上传接口即可。
      ,before: function(obj){
        //预读本地文件示例，不支持ie8
        obj.preview(function(index, file, result){
          $('#demo1').attr('src', result); //图片链接（base64）
        });
        
        element.progress('demo', '0%'); //进度条复位
        layer.msg('上传中', {icon: 16, time: 0});
      }
      ,done: function(res){
        //如果上传失败
        if(res.code > 0){
          return layer.msg('上传失败');
        }
        //上传成功的一些操作
        //……
        $('#demoText').html(''); //置空上传失败的状态
      }
      ,error: function(){
        //演示失败状态，并实现重传
        var demoText = $('#demoText');
        demoText.html('<span style="color: #FF5722;">上传失败</span> <a class="layui-btn layui-btn-xs demo-reload">重试</a>');
        demoText.find('.demo-reload').on('click', function(){
          uploadInst.upload();
        });
      }
      //进度条
      ,progress: function(n, elem, e){
        element.progress('demo', n + '%'); //可配合 layui 进度条元素使用
        if(n == 100){
          layer.msg('上传完毕', {icon: 1});
        }
      }
    });
  });


  layui.use('laydate', function(){
    var laydate = layui.laydate;
    
    //日期范围
    laydate.render({
      elem: '#test6'
      //设置开始日期、日期日期的 input 选择器
      //数组格式为 2.6.6 开始新增，之前版本直接配置 true 或任意分割字符即可
      ,range: ['#test-startDate-1', '#test-endDate-1']
    });
    
    
  });


  layui.use(['dropdown', 'util', 'layer', 'table'], function(){
    var dropdown = layui.dropdown
    ,util = layui.util
    ,layer = layui.layer
    ,table = layui.table
    ,$ = layui.jquery;
    //初演示 - 绑定输入框
    dropdown.render({
      elem: '#demo2'
      ,data: [{
        title: '机票订单'
        ,id: 101
      },{
        title: '数字订单'
        ,id: 102
      },{
        title: '理财订单'
        ,id: 103
      },{
        title: '网游订单'
        ,id: 104
      },{
        title: '酒店订单'
        ,id: 105
      },{
        title: '保险订单'
        ,id: 106
      }]
      ,click: function(obj){
        this.elem.val(obj.title);
      }
      ,style: 'width: 180px;'
    });

    dropdown.render({
      elem: '#demo3'
      ,data: [{
        title: '需我评价'
        ,id: 101
      },{
        title: '我已评价'
        ,id: 102
      },{
        title: '对方已评'
        ,id: 103
      },{
        title: '双方互评'
        ,id: 104
      }]
      ,click: function(obj){
        this.elem.val(obj.title);
      }
      ,style: 'width: 180px;'
    });

    dropdown.render({
      elem: '#demo4'
      ,data: [{
        title: '等待买家付款'
        ,id: 101
      },{
        title: '付款确认中'
        ,id: 102
      },{
        title: '买家已付款'
        ,id: 103
      },{
        title: '卖家已发货'
        ,id: 104
      },{
        title: '交易成功'
        ,id: 105
      },{
        title: '交易关闭'
        ,id: 106
      },{
        title: '退款中的订单'
        ,id: 107
      }]
      ,click: function(obj){
        this.elem.val(obj.title);
      }
      ,style: 'width: 180px;'
    });

    dropdown.render({
      elem: '#demo5'
      ,data: [{
        title: '已投诉'
        ,id: 101
      },{
        title: '退款中'
        ,id: 102
      }]
      ,click: function(obj){
        this.elem.val(obj.title);
      }
      ,style: 'width: 180px;'
    });
    
    
  });






 